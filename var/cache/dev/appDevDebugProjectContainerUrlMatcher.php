<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);

        // _assetic_css
        if ('/assetic/css' === $pathinfo) {
            return array (  '_controller' => 'assetic.controller:render',  'name' => 'css',  'pos' => NULL,  '_route' => '_assetic_css',);
        }

        if (0 === strpos($pathinfo, '/css/e2eafb2')) {
            // _assetic_e2eafb2
            if ('/css/e2eafb2.css' === $pathinfo) {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'e2eafb2',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_e2eafb2',);
            }

            if (0 === strpos($pathinfo, '/css/e2eafb2_')) {
                // _assetic_e2eafb2_0
                if ('/css/e2eafb2_bootstrap.min_1.css' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'e2eafb2',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_e2eafb2_0',);
                }

                // _assetic_e2eafb2_1
                if ('/css/e2eafb2_rating_2.css' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'e2eafb2',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_e2eafb2_1',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/7611230')) {
            // _assetic_7611230
            if ('/js/7611230.js' === $pathinfo) {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 7611230,  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_7611230',);
            }

            if (0 === strpos($pathinfo, '/js/7611230_')) {
                // _assetic_7611230_0
                if ('/js/7611230_bootstrap.min_1.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 7611230,  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_7611230_0',);
                }

                // _assetic_7611230_1
                if ('/js/7611230_rating_2.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 7611230,  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_7611230_1',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === rtrim($pathinfo, '/')) {
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                        goto not__profiler_home;
                    } else {
                        return $this->redirect($rawPathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ('/_profiler/purge' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/api/v1')) {
            // api_review_get_reviews
            if (0 === strpos($pathinfo, '/api/v1/reviews') && preg_match('#^/api/v1/reviews/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#sD', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_api_review_get_reviews;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_review_get_reviews')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::getReviewsAction',  '_format' => 'json',));
            }
            not_api_review_get_reviews:

            if (0 === strpos($pathinfo, '/api/v1/books')) {
                // api_review_get_book_reviews
                if (preg_match('#^/api/v1/books/(?P<bookId>[^/]++)/reviews(?:\\.(?P<_format>json|xml|html))?$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_review_get_book_reviews;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_review_get_book_reviews')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::getBookReviewsAction',  '_format' => 'json',));
                }
                not_api_review_get_book_reviews:

                // api_review_get_book_review
                if (preg_match('#^/api/v1/books/(?P<bookId>[^/]++)/reviews/(?P<reviewId>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_review_get_book_review;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_review_get_book_review')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::getBookReviewAction',  '_format' => 'json',));
                }
                not_api_review_get_book_review:

            }

            // api_review_delete_reviews
            if (0 === strpos($pathinfo, '/api/v1/reviews') && preg_match('#^/api/v1/reviews/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#sD', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_api_review_delete_reviews;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_review_delete_reviews')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::deleteReviewsAction',  '_format' => 'json',));
            }
            not_api_review_delete_reviews:

            // api_review_delete_book_reviews
            if (0 === strpos($pathinfo, '/api/v1/books') && preg_match('#^/api/v1/books/(?P<bookId>[^/]++)/reviews/(?P<reviewId>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#sD', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_api_review_delete_book_reviews;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_review_delete_book_reviews')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::deleteBookReviewsAction',  '_format' => 'json',));
            }
            not_api_review_delete_book_reviews:

            // api_review_put_review
            if (0 === strpos($pathinfo, '/api/v1/reviews') && preg_match('#^/api/v1/reviews/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#sD', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_api_review_put_review;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_review_put_review')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::putReviewAction',  '_format' => 'json',));
            }
            not_api_review_put_review:

            if (0 === strpos($pathinfo, '/api/v1/books')) {
                // api_review_put_book_review
                if (preg_match('#^/api/v1/books/(?P<bookId>[^/]++)/reviews/(?P<reviewId>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#sD', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_api_review_put_book_review;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_review_put_book_review')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::putBookReviewAction',  '_format' => 'json',));
                }
                not_api_review_put_book_review:

                // api_review_post_book_review
                if (preg_match('#^/api/v1/books/(?P<bookId>[^/]++)/reviews(?:\\.(?P<_format>json|xml|html))?$#sD', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_api_review_post_book_review;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_review_post_book_review')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::postBookReviewAction',  '_format' => 'json',));
                }
                not_api_review_post_book_review:

            }

        }

        // book_review_core_homepage
        if ('' === rtrim($pathinfo, '/')) {
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                goto not_book_review_core_homepage;
            } else {
                return $this->redirect($rawPathinfo.'/', 'book_review_core_homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'book_review_core_homepage',);
        }
        not_book_review_core_homepage:

        // book_review_core_about
        if ('/about' === $pathinfo) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_book_review_core_about;
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::aboutAction',  '_route' => 'book_review_core_about',);
        }
        not_book_review_core_about:

        if (0 === strpos($pathinfo, '/book')) {
            if (0 === strpos($pathinfo, '/books')) {
                // google_api_search
                if ('/books/search' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::googleApiSearchAction',  '_route' => 'google_api_search',);
                }

                // google_api_results
                if ('/books/results' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::googleApiResultsAction',  '_route' => 'google_api_results',);
                }

            }

            // google_api_view
            if ('/book/view' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::googleApiViewAction',  '_route' => 'google_api_view',);
            }

            if (0 === strpos($pathinfo, '/books')) {
                // book_books
                if ('/books' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'book_books',);
                }

                // book_review_book_view
                if (preg_match('#^/books/(?P<id>\\d+)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'book_review_book_view')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::viewAction',));
                }

                // book_review_book_create
                if ('/books/create' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::createAction',  '_route' => 'book_review_book_create',);
                }

                // book_review
                if (preg_match('#^/books/(?P<id>\\d+)/(?P<bookId>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'book_review')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::reviewAction',));
                }

            }

        }

        // book_user_review
        if (0 === strpos($pathinfo, '/review') && preg_match('#^/review/(?P<id>\\d+)/(?P<bookId>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'book_user_review')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::usereviewAction',));
        }

        // moderator
        if ('/moderator' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\ModeratorController::indexAction',  '_route' => 'moderator',);
        }

        // search
        if ('/search' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\PostController::handleSearchAction',  '_route' => 'search',);
        }

        // book_home
        if ('' === rtrim($pathinfo, '/')) {
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                goto not_book_home;
            } else {
                return $this->redirect($rawPathinfo.'/', 'book_home');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'book_home',);
        }
        not_book_home:

        // delete_review
        if ('/moderator/delete/review' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\postController::deleteReviewAction',  '_route' => 'delete_review',);
        }

        // book_review_about
        if ('/about' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::aboutAction',  '_route' => 'book_review_about',);
        }

        if (0 === strpos($pathinfo, '/moderator')) {
            // moderator_users
            if ('/moderator/users' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\ModeratorController::indexAction',  '_route' => 'moderator_users',);
            }

            // moderator_block_user
            if ('/moderator/block' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\ModeratorController::blockUserAction',  '_route' => 'moderator_block_user',);
            }

            // moderator_unblock_user
            if ('/moderator/unblock' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\ModeratorController::unblockUserAction',  '_route' => 'moderator_unblock_user',);
            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ('/login' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'fos_user.security.controller:loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ('/login_check' === $pathinfo) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'fos_user.security.controller:checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ('/logout' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'fos_user.security.controller:logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if ('/profile' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not_fos_user_profile_show;
                } else {
                    return $this->redirect($rawPathinfo.'/', 'fos_user_profile_show');
                }

                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                return array (  '_controller' => 'fos_user.profile.controller:showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ('/profile/edit' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'fos_user.profile.controller:editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if ('/register' === rtrim($pathinfo, '/')) {
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                        goto not_fos_user_registration_register;
                    } else {
                        return $this->redirect($rawPathinfo.'/', 'fos_user_registration_register');
                    }

                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    return array (  '_controller' => 'fos_user.registration.controller:registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ('/register/check-email' === $pathinfo) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'fos_user.registration.controller:checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'fos_user.registration.controller:confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ('/register/confirmed' === $pathinfo) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'fos_user.registration.controller:confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ('/resetting/request' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'fos_user.resetting.controller:requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ('/resetting/send-email' === $pathinfo) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'fos_user.resetting.controller:sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ('/resetting/check-email' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'fos_user.resetting.controller:checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'fos_user.resetting.controller:resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ('/profile/change-password' === $pathinfo) {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'fos_user.change_password.controller:changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        // dcs_rating_add_vote
        if (0 === strpos($pathinfo, '/vote/add') && preg_match('#^/vote/add/(?P<id>[^/]++)/(?P<value>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'dcs_rating_add_vote')), array (  '_controller' => 'DCS\\RatingBundle\\Controller\\RatingController::addVoteAction',));
        }

        if (0 === strpos($pathinfo, '/admin')) {
            // easyadmin
            if ('/admin' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not_easyadmin;
                } else {
                    return $this->redirect($rawPathinfo.'/', 'easyadmin');
                }

                return array (  '_controller' => 'EasyCorp\\Bundle\\EasyAdminBundle\\Controller\\AdminController::indexAction',  '_route' => 'easyadmin',);
            }
            not_easyadmin:

            // admin
            if ('/admin' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not_admin;
                } else {
                    return $this->redirect($rawPathinfo.'/', 'admin');
                }

                return array (  '_controller' => 'EasyCorp\\Bundle\\EasyAdminBundle\\Controller\\AdminController::indexAction',  '_route' => 'admin',);
            }
            not_admin:

        }

        if (0 === strpos($pathinfo, '/oauth/v2')) {
            // fos_oauth_server_token
            if ('/oauth/v2/token' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_oauth_server_token;
                }

                return array (  '_controller' => 'fos_oauth_server.controller.token:tokenAction',  '_route' => 'fos_oauth_server_token',);
            }
            not_fos_oauth_server_token:

            // fos_oauth_server_authorize
            if ('/oauth/v2/auth' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_oauth_server_authorize;
                }

                return array (  '_controller' => 'fos_oauth_server.controller.authorize:authorizeAction',  '_route' => 'fos_oauth_server_authorize',);
            }
            not_fos_oauth_server_authorize:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
