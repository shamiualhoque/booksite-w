<?php

/* AppBundle:Default:view.html.twig */
class __TwigTemplate_e4d2790fab4780dbd5fa2e34b25f305870edf74b366de61c71f5544fcb047e23 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:view.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Default:view.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Books View";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "    <div class=\"row\" style=\"margin-top: 20px\">
        <div class=\"col-sm-3\">
            ";
        // line 10
        echo "            <div class=\"panel-group\">
                <div class=\"panel panel-primary\">
                    <div class=\"panel-heading\">Search</div>
                    <div class=\"panel-body\">";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\HttpKernelExtension']->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\HttpKernelExtension']->controller("AppBundle:Post:search"));
        echo "</div>
                </div>
                <div class=\"panel panel-primary\">
                    <div class=\"panel-heading\">Find by Genre</div>
                    <div class=\"panel-body\">
                        <ul style=\"list-style: none;\">
                            ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["genres"]) || array_key_exists("genres", $context) ? $context["genres"] : (function () { throw new Twig_Error_Runtime('Variable "genres" does not exist.', 19, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["genreItems"]) {
            // line 20
            echo "                                <li style=\"margin-top: 10px;\"><a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("book_review_book_view", array("id" => twig_get_attribute($this->env, $this->source, $context["genreItems"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["genreItems"], "genre", array()), "html", null, true);
            echo "</a></li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['genreItems'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "                        </ul>

                    </div>
                </div>
            </div>

        </div>
        <div class=\"col-sm-9\">
            ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["books"]) || array_key_exists("books", $context) ? $context["books"] : (function () { throw new Twig_Error_Runtime('Variable "books" does not exist.', 30, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["book"]) {
            // line 31
            echo "                <div class=\"col-sm-3\" style=\"margin-top: 40px; \">
                    <img class=\"img-responsive\" style=\"height:200px \" src=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/book_covers/" . twig_get_attribute($this->env, $this->source, $context["book"], "image", array()))), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["book"], "title", array()), "html", null, true);
            echo " book cover\"/>
                    <a href=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("book_review", array("id" => (isset($context["id"]) || array_key_exists("id", $context) ? $context["id"] : (function () { throw new Twig_Error_Runtime('Variable "id" does not exist.', 33, $this->source); })()), "bookId" => twig_get_attribute($this->env, $this->source, $context["book"], "id", array()))), "html", null, true);
            echo "\">
                        ";
            // line 34
            echo twig_escape_filter($this->env, (((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["book"], "title", array())) > 20)) ? ((twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["book"], "title", array()), 0, 20) . "...")) : (twig_get_attribute($this->env, $this->source, $context["book"], "title", array()))), "html", null, true);
            echo "
                    </a>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['book'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Default:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 38,  117 => 34,  113 => 33,  107 => 32,  104 => 31,  100 => 30,  90 => 22,  79 => 20,  75 => 19,  66 => 13,  61 => 10,  57 => 7,  51 => 6,  39 => 4,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% extends 'base.html.twig' %}

{% block title %}Books View{% endblock %}

{% block body %}
    <div class=\"row\" style=\"margin-top: 20px\">
        <div class=\"col-sm-3\">
            {#group sections#}
            <div class=\"panel-group\">
                <div class=\"panel panel-primary\">
                    <div class=\"panel-heading\">Search</div>
                    <div class=\"panel-body\">{{ render(controller('AppBundle:Post:search')) }}</div>
                </div>
                <div class=\"panel panel-primary\">
                    <div class=\"panel-heading\">Find by Genre</div>
                    <div class=\"panel-body\">
                        <ul style=\"list-style: none;\">
                            {% for genreItems in genres %}
                                <li style=\"margin-top: 10px;\"><a href=\"{{ path('book_review_book_view', {'id':genreItems.id}) }}\">{{ genreItems.genre }}</a></li>
                            {% endfor %}
                        </ul>

                    </div>
                </div>
            </div>

        </div>
        <div class=\"col-sm-9\">
            {% for book in books %}
                <div class=\"col-sm-3\" style=\"margin-top: 40px; \">
                    <img class=\"img-responsive\" style=\"height:200px \" src=\"{{ asset('uploads/book_covers/' ~ book.image) }}\" alt=\"{{ book.title }} book cover\"/>
                    <a href=\"{{ path('book_review', {'id': id, 'bookId': book.id}) }}\">
                        {{ book.title|length > 20 ? book.title|slice(0,20) ~ '...' : book.title}}
                    </a>
                </div>
            {% endfor %}
        </div>
    </div>

{% endblock %}

", "AppBundle:Default:view.html.twig", "/home/shamiual/Downloads/Hussain,Waeed#/src/AppBundle/Resources/views/Default/view.html.twig");
    }
}
