<?php

/* AppBundle:Default:review.html.twig */
class __TwigTemplate_aa2112f112474259b84e5cd722eabee6b6aaa0982a48e49073983d32e7d345e3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:review.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Default:review.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "About the Book";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "    <h1>About the Book</h1>
    <hr>
    <hr>
    <div class=\"row\" style=\"margin-top: 20px;\">
        <div class=\"col-sm-4\">
            <figure>
                <img src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/book_covers/" . twig_get_attribute($this->env, $this->source, (isset($context["bookInfo"]) || array_key_exists("bookInfo", $context) ? $context["bookInfo"] : (function () { throw new Twig_Error_Runtime('Variable "bookInfo" does not exist.', 13, $this->source); })()), "image", array()))), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bookInfo"]) || array_key_exists("bookInfo", $context) ? $context["bookInfo"] : (function () { throw new Twig_Error_Runtime('Variable "bookInfo" does not exist.', 13, $this->source); })()), "title", array()), "html", null, true);
        echo " book cover\">
            </figure>
        </div>
        <div class=\"col-sm-4\">
            <div class=\"row\">
                <h3><strong>Title</strong></h3>
                <p>";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bookInfo"]) || array_key_exists("bookInfo", $context) ? $context["bookInfo"] : (function () { throw new Twig_Error_Runtime('Variable "bookInfo" does not exist.', 19, $this->source); })()), "title", array()), "html", null, true);
        echo "</p>
            </div>
            <div class=\"row\">
                <h3><strong>Author</strong></h3>
                <p>";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bookInfo"]) || array_key_exists("bookInfo", $context) ? $context["bookInfo"] : (function () { throw new Twig_Error_Runtime('Variable "bookInfo" does not exist.', 23, $this->source); })()), "author", array()), "html", null, true);
        echo "</p>
            </div>
            <div class=\"row\">
                <h3><strong>Genre</strong></h3>
                <p>";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bookInfo"]) || array_key_exists("bookInfo", $context) ? $context["bookInfo"] : (function () { throw new Twig_Error_Runtime('Variable "bookInfo" does not exist.', 27, $this->source); })()), "genre", array()), "html", null, true);
        echo "</p>
            </div>
            <div class=\"row\">
                <h3><strong>Google Rating</strong></h3>
                <p>";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["googleRating"]) || array_key_exists("googleRating", $context) ? $context["googleRating"] : (function () { throw new Twig_Error_Runtime('Variable "googleRating" does not exist.', 31, $this->source); })()), "html", null, true);
        echo " / 5</p>
            </div>
        </div>
        <div class=\"col-sm-4\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">Average Review</div>
                <div class=\"panel-body\">
                    ";
        // line 38
        echo $this->extensions['blackknight467\StarRatingBundle\Extensions\StarRatingExtension']->rating((isset($context["total"]) || array_key_exists("total", $context) ? $context["total"] : (function () { throw new Twig_Error_Runtime('Variable "total" does not exist.', 38, $this->source); })()));
        echo "
                    ";
        // line 39
        if ((($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER") || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_MODERATOR"))) {
            // line 40
            echo "
                        <a class=\"btn-default btn btn-block\" href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("book_user_review", array("id" => (isset($context["id"]) || array_key_exists("id", $context) ? $context["id"] : (function () { throw new Twig_Error_Runtime('Variable "id" does not exist.', 41, $this->source); })()), "bookId" => twig_get_attribute($this->env, $this->source, (isset($context["bookInfo"]) || array_key_exists("bookInfo", $context) ? $context["bookInfo"] : (function () { throw new Twig_Error_Runtime('Variable "bookInfo" does not exist.', 41, $this->source); })()), "id", array()))), "html", null, true);
            echo "\">Leave A Review</a>
                    ";
        }
        // line 43
        echo "                </div>
            </div>
        </div>
    </div>

    <div class=\"row col-sm-12\" style=\"margin-top: 20px;\">
        <h2><strong>Synopsis</strong></h2>
        <p>";
        // line 50
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bookInfo"]) || array_key_exists("bookInfo", $context) ? $context["bookInfo"] : (function () { throw new Twig_Error_Runtime('Variable "bookInfo" does not exist.', 50, $this->source); })()), "synopsis", array()), "html", null, true);
        echo "</p>
    </div>


    <div class=\"row\" style=\"margin-top: 50px;\">
        <h3>Reviews</h3>
        ";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reviews"]) || array_key_exists("reviews", $context) ? $context["reviews"] : (function () { throw new Twig_Error_Runtime('Variable "reviews" does not exist.', 56, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
            // line 57
            echo "            <div style=\"margin-top:20px;background-color: #D3D3D3;border: solid black 5px;box-shadow: 10px 10px 5px grey;\" class=\"col-sm-6 col-xs-12\">
                <p>";
            // line 58
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["review"], "userId", array()), "firstname", array()), "html", null, true);
            echo "</p>
                <p>";
            // line 59
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["review"], "date", array()), "m/d/Y"), "html", null, true);
            echo "</p>
                ";
            // line 60
            echo $this->extensions['blackknight467\StarRatingBundle\Extensions\StarRatingExtension']->rating(twig_get_attribute($this->env, $this->source, $context["review"], "rating", array()));
            echo "
                <p>";
            // line 61
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["review"], "review", array()), "html", null, true);
            echo "</p>
                ";
            // line 62
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_MODERATOR")) {
                // line 63
                echo "                    <form method=\"post\" action=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("delete_review");
                echo "\">
                        <input type=\"hidden\" name=\"id\" value=\"";
                // line 64
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["review"], "id", array()), "html", null, true);
                echo "\">
                        <input type=\"submit\" value=\"Delete\">
                    </form>
                ";
            }
            // line 68
            echo "            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 70
        echo "    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Default:review.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 70,  176 => 68,  169 => 64,  164 => 63,  162 => 62,  158 => 61,  154 => 60,  150 => 59,  146 => 58,  143 => 57,  139 => 56,  130 => 50,  121 => 43,  116 => 41,  113 => 40,  111 => 39,  107 => 38,  97 => 31,  90 => 27,  83 => 23,  76 => 19,  65 => 13,  57 => 7,  51 => 6,  39 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}About the Book{% endblock %}


{% block body %}
    <h1>About the Book</h1>
    <hr>
    <hr>
    <div class=\"row\" style=\"margin-top: 20px;\">
        <div class=\"col-sm-4\">
            <figure>
                <img src=\"{{ asset('uploads/book_covers/' ~ bookInfo.image) }}\" alt=\"{{ bookInfo.title }} book cover\">
            </figure>
        </div>
        <div class=\"col-sm-4\">
            <div class=\"row\">
                <h3><strong>Title</strong></h3>
                <p>{{ bookInfo.title }}</p>
            </div>
            <div class=\"row\">
                <h3><strong>Author</strong></h3>
                <p>{{ bookInfo.author }}</p>
            </div>
            <div class=\"row\">
                <h3><strong>Genre</strong></h3>
                <p>{{ bookInfo.genre }}</p>
            </div>
            <div class=\"row\">
                <h3><strong>Google Rating</strong></h3>
                <p>{{ googleRating }} / 5</p>
            </div>
        </div>
        <div class=\"col-sm-4\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">Average Review</div>
                <div class=\"panel-body\">
                    {{ total|rating }}
                    {% if is_granted('ROLE_USER') or is_granted('ROLE_ADMIN') or is_granted('ROLE_MODERATOR') %}

                        <a class=\"btn-default btn btn-block\" href=\"{{ path('book_user_review', {'id': id, 'bookId': bookInfo.id}) }}\">Leave A Review</a>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>

    <div class=\"row col-sm-12\" style=\"margin-top: 20px;\">
        <h2><strong>Synopsis</strong></h2>
        <p>{{ bookInfo.synopsis}}</p>
    </div>


    <div class=\"row\" style=\"margin-top: 50px;\">
        <h3>Reviews</h3>
        {% for review in reviews %}
            <div style=\"margin-top:20px;background-color: #D3D3D3;border: solid black 5px;box-shadow: 10px 10px 5px grey;\" class=\"col-sm-6 col-xs-12\">
                <p>{{ review.userId.firstname }}</p>
                <p>{{ review.date|date(\"m/d/Y\") }}</p>
                {{ review.rating|rating }}
                <p>{{ review.review }}</p>
                {% if is_granted('ROLE_MODERATOR') %}
                    <form method=\"post\" action=\"{{ path('delete_review') }}\">
                        <input type=\"hidden\" name=\"id\" value=\"{{ review.id }}\">
                        <input type=\"submit\" value=\"Delete\">
                    </form>
                {% endif %}
            </div>
        {% endfor %}
    </div>
{% endblock %}", "AppBundle:Default:review.html.twig", "/home/shamiual/Downloads/Hussain,Waeed#/src/AppBundle/Resources/views/Default/review.html.twig");
    }
}
