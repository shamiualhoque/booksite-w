<?php

/* AppBundle:GoogleApi:view.html.twig */
class __TwigTemplate_770b419820f8b2684d30b008fbd69a96580e563d128204e151c645fb61fbfc37 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:GoogleApi:view.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:GoogleApi:view.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "About the Book";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "<h1>About the Book</h1>
<hr>
<hr>
<div class=\"row\" style=\"margin-top: 20px;\">
    <div class=\"col-sm-4\">
        <figure>
            <img src=\"";
        // line 13
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, ($context["bookInfo"] ?? null), "image", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["bookInfo"] ?? null), "image", array()), "")) : ("")), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bookInfo"]) || array_key_exists("bookInfo", $context) ? $context["bookInfo"] : (function () { throw new Twig_Error_Runtime('Variable "bookInfo" does not exist.', 13, $this->source); })()), "title", array()), "html", null, true);
        echo " book cover\">
        </figure>
    </div>
    <div class=\"col-sm-4\">
        <div class=\"row\">
            <h3><strong>Title</strong></h3>
            <p>";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bookInfo"]) || array_key_exists("bookInfo", $context) ? $context["bookInfo"] : (function () { throw new Twig_Error_Runtime('Variable "bookInfo" does not exist.', 19, $this->source); })()), "title", array()), "html", null, true);
        echo "</p>
        </div>
        <div class=\"row\">
            <h3><strong>Author</strong></h3>
            <p>";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bookInfo"]) || array_key_exists("bookInfo", $context) ? $context["bookInfo"] : (function () { throw new Twig_Error_Runtime('Variable "bookInfo" does not exist.', 23, $this->source); })()), "author", array()), "html", null, true);
        echo "</p>
        </div>
        <div class=\"row\">
            <h3><strong>Genre</strong></h3>
            <p>";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bookInfo"]) || array_key_exists("bookInfo", $context) ? $context["bookInfo"] : (function () { throw new Twig_Error_Runtime('Variable "bookInfo" does not exist.', 27, $this->source); })()), "genre", array()), "html", null, true);
        echo "</p>
        </div>
    </div>
</div>

<div class=\"row col-sm-12\" style=\"margin-top: 20px;\">
    <h2><strong>Synopsis</strong></h2>
    <p>";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bookInfo"]) || array_key_exists("bookInfo", $context) ? $context["bookInfo"] : (function () { throw new Twig_Error_Runtime('Variable "bookInfo" does not exist.', 34, $this->source); })()), "synopsis", array()), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:GoogleApi:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 34,  90 => 27,  83 => 23,  76 => 19,  65 => 13,  57 => 7,  51 => 6,  39 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}About the Book{% endblock %}


{% block body %}
<h1>About the Book</h1>
<hr>
<hr>
<div class=\"row\" style=\"margin-top: 20px;\">
    <div class=\"col-sm-4\">
        <figure>
            <img src=\"{{ bookInfo.image|default('') }}\" alt=\"{{ bookInfo.title }} book cover\">
        </figure>
    </div>
    <div class=\"col-sm-4\">
        <div class=\"row\">
            <h3><strong>Title</strong></h3>
            <p>{{ bookInfo.title }}</p>
        </div>
        <div class=\"row\">
            <h3><strong>Author</strong></h3>
            <p>{{ bookInfo.author }}</p>
        </div>
        <div class=\"row\">
            <h3><strong>Genre</strong></h3>
            <p>{{ bookInfo.genre }}</p>
        </div>
    </div>
</div>

<div class=\"row col-sm-12\" style=\"margin-top: 20px;\">
    <h2><strong>Synopsis</strong></h2>
    <p>{{ bookInfo.synopsis }}</p>
</div>
{% endblock %}", "AppBundle:GoogleApi:view.html.twig", "/home/shamiual/Downloads/Hussain,Waeed#/src/AppBundle/Resources/views/GoogleApi/view.html.twig");
    }
}
