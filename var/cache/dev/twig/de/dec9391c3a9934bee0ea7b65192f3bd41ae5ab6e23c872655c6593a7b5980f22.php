<?php

/* AppBundle:Default:index.html.twig */
class __TwigTemplate_aa62ceaa55088abfa08fd8efd6042aaa6716d5ab5bc590d6b454fce7606343f4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Index";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div style=\"margin-top: 20px;\" class=\"row\">
        <div class=\"col-sm-3\" style=\"margin-top: 20px;\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\"><h3>Quick Links</h3></div>
                <div class=\"panel-body\">
                    <h4><a href=\"";
        // line 11
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("search");
        echo "\">Find Books</a></h4>
                    <h4><a href=\"";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("book_review_about");
        echo "\">Who are Click Books?</a></h4>
                    <h4><a href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_security_login");
        echo "\">Login</a></h4>
                </div>
            </div>

        </div>
        <div class=\"col-sm-9\">
            <div class=\"row\">
                <h2>Top 5 Books</h2>
                <hr>
                <hr>
                ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["topFive"]) || array_key_exists("topFive", $context) ? $context["topFive"] : (function () { throw new Twig_Error_Runtime('Variable "topFive" does not exist.', 23, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["book"]) {
            // line 24
            echo "                    <div class=\"col-sm-2\">
                        <figure>
                            <img class=\"img-responsive\" style=\"height:200px \" src=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/book_covers/" . twig_get_attribute($this->env, $this->source, $context["book"], "image", array()))), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["book"], "title", array()), "html", null, true);
            echo " book cover\"/>
                        </figure>
                        <figcaption>
                            <p>";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["book"], "title", array()), "html", null, true);
            echo "</p>
                        </figcaption>
                        </a>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['book'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "            </div>

            <div class=\"row\" style=\"margin-top: 30px\">
                <h2>Highest Votes</h2>
                <hr>
                <hr>
                ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["topReview"]) || array_key_exists("topReview", $context) ? $context["topReview"] : (function () { throw new Twig_Error_Runtime('Variable "topReview" does not exist.', 40, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["reviews"]) {
            // line 41
            echo "                    <div class=\"col-sm-2\">
                        <figure>
                            <img class=\"img-responsive\" style=\"height:200px \" src=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/book_covers/" . twig_get_attribute($this->env, $this->source, $context["reviews"], "image", array()))), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reviews"], "title", array()), "html", null, true);
            echo " book cover\"/>
                        </figure>
                        <figcaption>
                            <p>";
            // line 46
            echo twig_escape_filter($this->env, (((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reviews"], "title", array())) > 20)) ? ((twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["reviews"], "title", array()), 0, 20) . "...")) : (twig_get_attribute($this->env, $this->source, $context["reviews"], "title", array()))), "html", null, true);
            echo "</p>
                            ";
            // line 47
            echo $this->extensions['blackknight467\StarRatingBundle\Extensions\StarRatingExtension']->rating(twig_get_attribute($this->env, $this->source, $context["reviews"], "sumRating", array()), 5, "fa-norm");
            echo "
                        </figcaption>
                        </a>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reviews'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "            </div>

        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 52,  140 => 47,  136 => 46,  128 => 43,  124 => 41,  120 => 40,  112 => 34,  101 => 29,  93 => 26,  89 => 24,  85 => 23,  72 => 13,  68 => 12,  64 => 11,  57 => 6,  51 => 5,  39 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}Index{% endblock %}

{% block body %}
    <div style=\"margin-top: 20px;\" class=\"row\">
        <div class=\"col-sm-3\" style=\"margin-top: 20px;\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\"><h3>Quick Links</h3></div>
                <div class=\"panel-body\">
                    <h4><a href=\"{{ path('search') }}\">Find Books</a></h4>
                    <h4><a href=\"{{ path('book_review_about') }}\">Who are Click Books?</a></h4>
                    <h4><a href=\"{{ path('fos_user_security_login') }}\">Login</a></h4>
                </div>
            </div>

        </div>
        <div class=\"col-sm-9\">
            <div class=\"row\">
                <h2>Top 5 Books</h2>
                <hr>
                <hr>
                {% for book in topFive %}
                    <div class=\"col-sm-2\">
                        <figure>
                            <img class=\"img-responsive\" style=\"height:200px \" src=\"{{ asset('uploads/book_covers/' ~ book.image) }}\" alt=\"{{ book.title }} book cover\"/>
                        </figure>
                        <figcaption>
                            <p>{{ book.title }}</p>
                        </figcaption>
                        </a>
                    </div>
                {% endfor %}
            </div>

            <div class=\"row\" style=\"margin-top: 30px\">
                <h2>Highest Votes</h2>
                <hr>
                <hr>
                {% for reviews in topReview %}
                    <div class=\"col-sm-2\">
                        <figure>
                            <img class=\"img-responsive\" style=\"height:200px \" src=\"{{ asset('uploads/book_covers/' ~ reviews.image) }}\" alt=\"{{ reviews.title }} book cover\"/>
                        </figure>
                        <figcaption>
                            <p>{{ reviews.title|length > 20 ? reviews.title|slice(0,20) ~ '...' : reviews.title}}</p>
                            {{ reviews.sumRating|rating(5,\"fa-norm\") }}
                        </figcaption>
                        </a>
                    </div>
                {% endfor %}
            </div>

        </div>
    </div>

{% endblock %}

", "AppBundle:Default:index.html.twig", "/home/shamiual/Downloads/Hussain,Waeed#/src/AppBundle/Resources/views/Default/index.html.twig");
    }
}
