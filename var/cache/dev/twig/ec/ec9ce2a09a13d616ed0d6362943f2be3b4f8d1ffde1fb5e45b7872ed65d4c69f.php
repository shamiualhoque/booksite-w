<?php

/* AppBundle:post:view.html.twig */
class __TwigTemplate_406227fd45b145780e6542e99b6fb2429d7b5cb1a23d833789fcaa6d49f196f3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:post:view.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:post:view.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Books View";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <div style=\"margin-top: 20px;\" class=\"row\">
        <div class=\"col-sm-3\">
            <div class=\"panel-group\">
                <div class=\"panel panel-primary\">
                    <div class=\"panel-heading\">Search</div>
                    <div class=\"panel-body\">";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\HttpKernelExtension']->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\HttpKernelExtension']->controller("AppBundle:Post:search"));
        echo "</div>
                </div>
                <div class=\"panel panel-primary\">
                    <div class=\"panel-heading\">Find by Genre</div>
                    <div class=\"panel-body\">
                        <ul style=\"list-style: none;\">
                            ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["genres"]) || array_key_exists("genres", $context) ? $context["genres"] : (function () { throw new Twig_Error_Runtime('Variable "genres" does not exist.', 18, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["genreItems"]) {
            // line 19
            echo "                                <li style=\"margin-top: 10px;\"><a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("book_review_book_view", array("id" => twig_get_attribute($this->env, $this->source, $context["genreItems"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["genreItems"], "genre", array()), "html", null, true);
            echo "</a></li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['genreItems'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "                        </ul>

                    </div>
                </div>
            </div>

        </div>
        <div class=\"col-sm-9\">
            ";
        // line 29
        if ((null === (isset($context["searchString"]) || array_key_exists("searchString", $context) ? $context["searchString"] : (function () { throw new Twig_Error_Runtime('Variable "searchString" does not exist.', 29, $this->source); })()))) {
            // line 30
            echo "                ";
            // line 31
            echo "            ";
        } else {
            // line 32
            echo "                ";
            // line 33
            echo "                <h4 class=\"italics\">Results From Search \"";
            echo twig_escape_filter($this->env, (isset($context["searchString"]) || array_key_exists("searchString", $context) ? $context["searchString"] : (function () { throw new Twig_Error_Runtime('Variable "searchString" does not exist.', 33, $this->source); })()), "html", null, true);
            echo "\"</h4>
                ";
            // line 34
            if (twig_test_empty((isset($context["books"]) || array_key_exists("books", $context) ? $context["books"] : (function () { throw new Twig_Error_Runtime('Variable "books" does not exist.', 34, $this->source); })()))) {
                // line 35
                echo "                    <h4 class=\"italics\">No Books where found from the search</h4>
                ";
            } else {
                // line 37
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["books"]) || array_key_exists("books", $context) ? $context["books"] : (function () { throw new Twig_Error_Runtime('Variable "books" does not exist.', 37, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["book"]) {
                    // line 38
                    echo "                        <div class=\"col-sm-3\" style=\"margin-top: 40px; \">
                            <img class=\"img-responsive\" style=\"height:200px \" src=\"";
                    // line 39
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/book_covers/" . twig_get_attribute($this->env, $this->source, $context["book"], "image", array()))), "html", null, true);
                    echo "\" alt=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["book"], "title", array()), "html", null, true);
                    echo " book cover\"/>
                            <a href=\"";
                    // line 40
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("book_review", array("id" => twig_get_attribute($this->env, $this->source, $context["book"], "genre", array()), "bookId" => twig_get_attribute($this->env, $this->source, $context["book"], "id", array()))), "html", null, true);
                    echo "\">
                                ";
                    // line 41
                    echo twig_escape_filter($this->env, (((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["book"], "title", array())) > 20)) ? ((twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["book"], "title", array()), 0, 20) . "...")) : (twig_get_attribute($this->env, $this->source, $context["book"], "title", array()))), "html", null, true);
                    echo "
                            </a>
                        </div>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['book'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 45
                echo "                ";
            }
            // line 46
            echo "            ";
        }
        // line 47
        echo "        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:post:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 47,  150 => 46,  147 => 45,  137 => 41,  133 => 40,  127 => 39,  124 => 38,  119 => 37,  115 => 35,  113 => 34,  108 => 33,  106 => 32,  103 => 31,  101 => 30,  99 => 29,  89 => 21,  78 => 19,  74 => 18,  65 => 12,  57 => 6,  51 => 5,  39 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}Books View{% endblock %}

{% block body %}

    <div style=\"margin-top: 20px;\" class=\"row\">
        <div class=\"col-sm-3\">
            <div class=\"panel-group\">
                <div class=\"panel panel-primary\">
                    <div class=\"panel-heading\">Search</div>
                    <div class=\"panel-body\">{{ render(controller('AppBundle:Post:search')) }}</div>
                </div>
                <div class=\"panel panel-primary\">
                    <div class=\"panel-heading\">Find by Genre</div>
                    <div class=\"panel-body\">
                        <ul style=\"list-style: none;\">
                            {% for genreItems in genres %}
                                <li style=\"margin-top: 10px;\"><a href=\"{{ path('book_review_book_view', {'id':genreItems.id}) }}\">{{ genreItems.genre }}</a></li>
                            {% endfor %}
                        </ul>

                    </div>
                </div>
            </div>

        </div>
        <div class=\"col-sm-9\">
            {% if searchString is null%}
                {#nothing searched#}
            {% else %}
                {#searched#}
                <h4 class=\"italics\">Results From Search \"{{ searchString }}\"</h4>
                {% if books is empty%}
                    <h4 class=\"italics\">No Books where found from the search</h4>
                {% else %}
                    {% for book in books %}
                        <div class=\"col-sm-3\" style=\"margin-top: 40px; \">
                            <img class=\"img-responsive\" style=\"height:200px \" src=\"{{ asset('uploads/book_covers/' ~ book.image) }}\" alt=\"{{ book.title }} book cover\"/>
                            <a href=\"{{ path('book_review', {'id': book.genre, 'bookId': book.id}) }}\">
                                {{ book.title|length > 20 ? book.title|slice(0,20) ~ '...' : book.title}}
                            </a>
                        </div>
                    {% endfor %}
                {% endif %}
            {% endif %}
        </div>
    </div>

{% endblock %}

", "AppBundle:post:view.html.twig", "/home/shamiual/Downloads/Hussain,Waeed#/src/AppBundle/Resources/views/post/view.html.twig");
    }
}
