<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Entity\Review;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends FOSRestController
{
    public function getReviewsAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        // casts id to int
        $id = (int) $id;

        if ($id == 0) {
            $view = $this->view("id should be a integer", 404);
            return $this->handleView($view);
        }

        $review =  $em->getRepository(Review::class)->find($id);

        if (!$review) {
            $view = $this->view("No reviews found", 404);
        } else {
            $view = $this->view($review);
        }

        return $this->handleView($view);
    }

    public function getBookReviewsAction($bookId)
    {
        $em = $this->getDoctrine()->getManager();

        if ((int) $bookId == 0) {
            $view = $this->view("book ID should be a integer", 404);
            return $this->handleView($view);
        }

        $book = $em->getRepository(Book::class)->find($bookId);

        if (!$book) {
            $view =  $this->view("No Book was found", 404);
            return $this->handleView($view);
        }

        $review =  $em->getRepository(Review::class)->findBy(['book_id' => $book->getId()]);

        if (!$review) {
            $view = $this->view(null, 404);
        } else {
            $view = $this->view($review);
        }

        return $this->handleView($view);
    }

    public function getBookReviewAction($bookId, $reviewId)
    {
        $bookId = (int) $bookId;
        $reviewId = (int) $reviewId;

        if ($bookId == 0) {
            $view =  $this->view("BookId needs to a integer", 404);
            return $this->handleView($view);
        }

        if ($reviewId == 0) {
            $view =  $this->view("reviewId needs to a integer", 404);
            return $this->handleView($view);
        }

        $em = $this->getDoctrine()->getManager();

        $book = $em->getRepository(Book::class)->find($bookId);

        if (!$book) {
            $view =  $this->view("No Book was found", 404);
            return $this->handleView($view);
        }

        $review =  $em->getRepository(Review::class)->find($reviewId);

        if (!$review) {
            $view = $this->view("No reviews", 404);
        } else {
            $view = $this->view($review);
        }

        return $this->handleView($view);
    }

    public function deleteReviewsAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = (int) $id;

        if ($id == 0) {
            $view = $this->view("reviewId should be an integer", 404);
            $this->handleView($view);
        }

        $userId = $this->getUser()->getId();

        $review =  $em->getRepository(Review::class)->find($id);

        if (!$review) {
            $view = $this->view("No reviews", 401);
            return $this->handleView($view);
        }

        $reviewUserId = $review->getUserId()->getId();

        if ($reviewUserId != $userId) {
            $view = $this->view("Unauthorised to delete review", 401);
            return $this->handleView($view);
        }


        if (!$review) {
            $view = $this->view("Review doesn't exist", 404);
        } else {
            // todo soft delete
            $em->remove($review);
            $em->flush();
            $view = $this->view($review);
        }

        return $this->handleView($view);
    }

    public function deleteBookReviewsAction($bookId, $reviewId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $bookId = (int) $bookId;
        $reviewId = (int) $reviewId;

        if ($bookId == 0) {
            $view = $this->view("bookId should be an integer", 404);
            $this->handleView($view);
        }

        if ($reviewId == 0) {
            $view = $this->view("reviewId should be an integer", 404);
            $this->handleView($view);
        }

        $book = $em->getRepository(Book::class)->find($bookId);

        if (!$book) {
            $view =  $this->view("No Book was found", 404);
            return $this->handleView($view);
        }

        $userId = $this->getUser()->getId();

        $review =  $em->getRepository(Review::class)->find($reviewId);

        if (!$review) {
            $view =  $this->view("No reviews found", 404);
            return $this->handleView($view);
        }

        $reviewUserId = $review->getUserId()->getId();

        if ($reviewUserId != $userId) {
            $view = $this->view("Unauthorised to delete review", 401);
            return $this->handleView($view);
        }

        if (!$review) {
            $view = $this->view("Review doesn't exist", 404);
        } else {
            // todo soft delete
            $em->remove($review);
            $em->flush();
            $view = $this->view($review);
        }

        return $this->handleView($view);
    }

    public function putReviewAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = (int) $id;

        if ($id == 0) {
            $view = $this->view("Review doesn't exist", 404);
            return $this->handleView($view);
        }

        if($request->getContentType() != 'json') {
            return $this->handleView($this->view(null, 400));
        }

        $userId = $this->getUser()->getId();

        $review =  $em->getRepository(Review::class)->find($id);

        if (!$review) {
            $view = $this->view("No reviews", 404);
            return $this->handleView($view);
        }

        $reviewUserId = $review->getUserId()->getId();

        if ($reviewUserId != $userId) {
            $view = $this->view("Unauthorised to edit review", 401);
            return $this->handleView($view);
        }

        $params = $request->request->all();

        if (isset($params['review'])) {
            $review->setReview($params['review']);
        }

        if (isset($params['rating'])) {
            if ((int) $params['rating'] == 0) {
                $view = $this->view("rating should be an integer", 400);
                return $this->handleView($view);
            }
            if ($params['rating'] < 0 || $params['rating'] > 5) {
                $view = $this->view("rating should be an between 0 to 5", 400);
                return $this->handleView($view);
            }
            if ($params['rating'] )
                $review->setRating($params['rating']);
        }

        $em->persist($review);
        $em->flush();

        return $this->handleView($this->view(null, 200)
            ->setLocation(
                $this->generateUrl('api_review_get_reviews',
                    ['id' => $review->getId()]
                ) )
        );
    }

    public function putBookReviewAction($bookId, $reviewId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $bookId = (int) $bookId;
        $reviewId = (int) $reviewId;

        if ($bookId == 0) {
            $view = $this->view("Book doesn't exist", 404);
            return $this->handleView($view);
        }

        if ($reviewId == 0) {
            $view = $this->view("Review doesn't exist", 404);
            return $this->handleView($view);
        }

        if($request->getContentType() != 'json') {
            return $this->handleView($this->view(null, 400));
        }


        $userId = $this->getUser()->getId();

        $review =  $em->getRepository(Review::class)->find($reviewId);

        if (!$review) {
            $view = $this->view("No review", 401);
            return $this->handleView($view);
        }

        $reviewUserId = $review->getUserId()->getId();

        if ($reviewUserId != $userId) {
            $view = $this->view("Unauthorised to edit review", 401);
            return $this->handleView($view);
        }

        $params = $request->request->all();

        if (isset($params['review'])) {
            $review->setReview($params['review']);
        }

        if (isset($params['rating'])) {
            if ((int) $params['rating'] == 0) {
                $view = $this->view("rating should be an integer", 400);
                return $this->handleView($view);
            }
            if ($params['rating'] < 0 || $params['rating'] > 5) {
                $view = $this->view("rating should be an between 0 to 5", 400);
                return $this->handleView($view);
            }
            if ($params['rating'] )
                $review->setRating($params['rating']);
        }

        $em->persist($review);
        $em->flush();

        // todo: works but does not show json ??
        return $this->handleView($this->view(null, 200)
            ->setLocation(
                $this->generateUrl('api_review_get_reviews',
                    ['id' => $review->getId()]
                ) )
        );
    }

    public function postBookReviewAction($bookId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if($request->getContentType() != 'json') {
            return $this->handleView($this->view(null, 400));
        }

        $book = $em->getRepository(Book::class)->find($bookId);

        if (!$book) {
            $view = $this->view("Book doesn't exist", 401);
            return $this->handleView($view);
        }

        $review = new Review();

        $params = $request->request->all();

        $review->setUserId($this->getUser());
        $review->setDate(new DateTime());
        $review->setBookId($book);

        if (isset($params['review'])) {
            $review->setReview($params['review']);
        } else {
            $view = $this->view("No review provided", 401);
            return $this->handleView($view);
        }

        if (isset($params['rating'])) {
            if ((int) $params['rating'] == 0) {
                $view = $this->view("rating should be an integer", 400);
                return $this->handleView($view);
            }

            if ($params['rating'] < 1 || $params['rating'] > 5) {
                $view = $this->view("rating should be an between 1 to 5", 400);
                return $this->handleView($view);
            } else {
                $review->setRating($params['rating']);
            }
        }

        $em->persist($review);
        $em->flush();

        return $this->handleView($this->view("Review added", 200)
            ->setLocation(
                $this->generateUrl('api_review_get_reviews',
                    ['id' => $review->getId()]
                ) )
        );
    }
}
