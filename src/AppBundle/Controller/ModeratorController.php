<?php

namespace AppBundle\Controller;

use AppBundle\Service\ModeratorManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ModeratorController extends Controller
{
    public function indexAction(Request $request)
    {
        $bookManager = $this->container->get('moderator_manager');

        $users = $bookManager->getUsers();

        return $this->render('AppBundle:Moderator:index.html.twig', ['users' => $users]);
    }

    public function blockUserAction(Request $request)
    {
        $moderatorManager = $this->getModeratorManager();

        $userId = $request->query->get('userId');

        $moderatorManager->blockUser($userId);

        $users = $moderatorManager->getUsers();

        return $this->render('AppBundle:Moderator:index.html.twig', ['users' => $users]);
    }


    public function unblockUserAction(Request $request)
    {
        $moderatorManager = $this->getModeratorManager();

        $userId = $request->query->get('userId');

        $moderatorManager->unblockUser($userId);

        $users = $moderatorManager->getUsers();

        return $this->render('AppBundle:Moderator:index.html.twig', ['users' => $users]);
    }

    /**
     * @return ModeratorManager
     */
    private function getModeratorManager()
    {
        return $this->container->get('moderator_manager');
    }

}