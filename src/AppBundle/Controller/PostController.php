<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class PostController extends Controller
{
    public function searchAction()
    {
        $form = $this->createFormBuilder(null)
            ->add('search', TextType::class)
            ->getForm();

        return $this->render('AppBundle:post:search.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     */
    public function handleSearchAction(Request $request)
    {
        $bookManager = $this->container->get('book_manager');
        $search = $request->request->get('form')['search'];

        $searchManager =  $this->container->get('search_manager');

        $genre = $bookManager->getAllBookGenresWithId();

        $books = $searchManager->getBooksFromSearch($search);

        return $this->render('AppBundle:post:view.html.twig',
            [
                'searchString' => $search,
                'books' => $books,
                'genres' => $genre
            ]
        );
    }

    public function deleteReviewAction(Request $request)
    {
        $reviewId = $request->request->get('id');

        $bookManager = $this->getBookManagerContainer();

        $bookManager->deleteBookReview($reviewId);

        return ;
    }

    private function getBookManagerContainer()
    {
        return $this->container->get('book_manager');
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->container->get('doctrine.orm.default_entity_manager');
    }


}
