<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Entity\Review;
use AppBundle\Form\BookType;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $bookManager = $this->container->get('book_manager');

        $top5 = $bookManager->getTopFiveBooks();
        $highReview = $bookManager->getTopReviewBooks();

        return $this->render('AppBundle:Default:index.html.twig',['topFive' => $top5,'topReview'=>$highReview]);
    }

    public function googleApiSearchAction(Request $request)
    {
        $form = $this->createFormBuilder(null)
            ->add('search', TextType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        return $this->render(
            'AppBundle:GoogleApi:search.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function googleApiResultsAction(Request $request)
    {
        $form = $request->request->get('form');
        $search = $form['search'];

        $googleApiManager = $this->container->get('google_api');

        $books = $googleApiManager->getBooksFromGoogleApi($search);

        return $this->render('AppBundle:GoogleApi:results.html.twig', [
            'books' => $books
        ]);
    }

    public function googleApiViewAction(Request $request)
    {
        $isbn = $request->query->get('isbn');

        $googleApiManager = $this->container->get('google_api');

        $bookInfo = $googleApiManager->getBookInfoFromISBN($isbn);

        return $this->render('AppBundle:GoogleApi:view.html.twig', [
            'bookInfo' => $bookInfo
        ]);
    }

    public function viewAction($id)
    {
        $bookManager = $this->getBookManagerContainer();

        $genre = $bookManager->getGenreFromId($id);

        $genreList = $bookManager->getAllBookGenresWithId();

        $books = $bookManager->getBooksFromGenre($genre);

        return $this->render('AppBundle:Default:view.html.twig',
            [
                'id' => $id,
                'books' => $books,
                'genres' =>$genreList
            ]
        );
    }

    public function createAction(Request $request)
    {
        $entityManger = $this->getDoctrine()->getManager();
        $bookManager = $this->container->get('book_manager');
        $bookEntry = new Book();

        $form = $this->createForm(BookType::class, $bookEntry,
            ['action' => $request->getUri(), 'book_manager' => $bookManager]);

        $form->handleRequest($request);

        $returnView = $this->render('AppBundle:Default:create.html.twig',
            ['form' => $form->createView()]);

        if($form->isValid()) {
            $bookEntry->setTitle($bookEntry->getTitle());
            $bookEntry->setAuthor($bookEntry->getAuthor());
            $bookEntry->setSynopsis($bookEntry->getSynopsis());
            $bookEntry->setViews();
            $bookEntry->setVisible(false);
            $bookEntry->setIsbn($bookEntry->getIsbn());
            $bookEntry->setGenre($bookManager->getGenreFromId($bookEntry->getGenre()));

            /** @var UploadedFile $image */
            $image = $bookEntry->getImage();

            $filename = md5(uniqid()).'.'.$image->guessExtension();

            $image->move(
                $this->getParameter('book_cover_directory'),
                $filename
            );

            $bookEntry->setImage($filename);

            $entityManger->persist($bookEntry);
            $entityManger->flush();
            $returnView = $this->redirectToRoute("search");

        }

        return $returnView;
    }

    public function aboutAction()
    {
        return $this->render('AppBundle:Default:about.html.twig');
    }


    public function reviewAction($id, $bookId)
    {
        $bookManager = $this->getBookManagerContainer();
        $googleAPIManager = $this->container->get('google_api');

        $bookInfo = $bookManager->getBookFromId($bookId);

        $bookReviews = $bookManager->getBooksReviews($bookId, 10, 0);
        $count = count($bookReviews);
        $totalStars = 0;
        if($count>0)
        {
            $totalStars = $bookManager->getSumBook($bookId)[0][1]/$count;
        }

        $googleRating = $googleAPIManager->getBookRating($bookInfo->getTitle());

        $bookManager->updateHit($bookId);
        return $this->render('AppBundle:Default:review.html.twig',
            [
                'id' => $id,
                'bookInfo' => $bookInfo,
                'bookId' => $bookId,
                'reviews' => $bookReviews,
                'total' => $totalStars,
                'googleRating' => $googleRating
            ]
        );
    }

    public function usereviewAction($id, $bookId, Request $request)
    {
        $reviewEntry = new Review();
        $form = $this->createForm(ReviewType::class, $reviewEntry,
            [
                'action' => $request->getUri()
            ]
        );

        $form->handleRequest($request);
        $returnView = $this->render('AppBundle:Default:usereview.html.twig',['form' => $form->createView()]);

        if($form->isValid()) {
            $em = $this->getEntityManager();

            $book = $this->getBookManagerContainer()->getBookFromId($bookId);

            $reviewEntry->setUserId($this->getUser());
            $reviewEntry->setDate(new DateTime('now'));
            $reviewEntry->setRating($reviewEntry->getRating());
            $reviewEntry->setReview($reviewEntry->getReview());
            $reviewEntry->setBookId($book);

            $em->persist($reviewEntry);
            $em->flush($reviewEntry);
            $returnView = $this->redirectToRoute("book_review",array('bookId'=>$bookId,'id'=>$id));
        }
        return $returnView;
    }

    /**
     * @return BookManager|object
     */
    private function getBookManagerContainer()
    {
        return $this->container->get('book_manager');
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->container->get('doctrine.orm.default_entity_manager');
    }
}
