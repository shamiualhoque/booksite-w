<?php

namespace AppBundle\Form;

use AppBundle\Service\BookManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var BookManager $bookManager */
        $bookManager = $options['book_manager'];

        $genreArray = $bookManager->getAllBookGenresWithIdAsValue();

        $builder
            ->add('title')
            ->add('author')
            ->add('synopsis')
            ->add('isbn')
            ->add('genre', ChoiceType::class, [
                'choices' => [
                    'Genre\'s' => $genreArray
                ]
            ])
            ->add('image', FileType::class, ['data_class' => null])
            ->add('submit', SubmitType::class);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Book'
        ));

        $resolver->setRequired('book_manager');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bookreview_bookbundle_book';
    }


}
