<?php

namespace AppBundle\Service;

use AppBundle\User;
use Doctrine\ORM\EntityManager;

class ModeratorManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * BookManager constructor.
     *
     * @param $entityManager EntityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getUsers()
    {
        $em = $this->getEntityManager();

        $users = $em->getRepository(User::class)->findAll();

        return $users;
    }

    public function blockUser($userId)
    {
        $em = $this->getEntityManager();

        $user = $em->getRepository(User::class)->find($userId);
        $user->setEnabled(false);

        $em->persist($user);
        $em->flush();
    }

    public function unblockUser($userId)
    {
        $em = $this->getEntityManager();

        $user = $em->getRepository(User::class)->find($userId);
        $user->setEnabled(true);

        $em->persist($user);
        $em->flush();
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->entityManager;
    }
}