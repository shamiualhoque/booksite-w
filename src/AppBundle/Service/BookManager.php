<?php

namespace AppBundle\Service;


use AppBundle\Entity\Book;
use AppBundle\Entity\Genre;
use AppBundle\Entity\Review;
use Doctrine\ORM\EntityManager;

class BookManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * BookManager constructor.
     *
     * @param $entityManager EntityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public function updateHit($id)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery('UPDATE AppBundle:Book b SET b.views=b.views+1 WHERE b.id=:iden');
        $query->setParameter('iden',$id)->execute();
    }

    public function deleteBookReview($reviewId)
    {
        $em = $this->getEntityManager();

        $result = $em->createQuery('UPDATE AppBundle:Review r SET r.soft_delete = TRUE WHERE r.id = :review_id')
            ->setParameter('review_id', $reviewId);

        $result->execute();
    }

    /**
     * Returns all Books genres
     *
     * @return array
     */
    public function getAllBookGenresWithId()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT g.id, g.genre from AppBundle:Genre g');

        return $query->getResult();
    }

    public function getAllBookGenresWithIdAsValue()
    {
        $em = $this->getEntityManager();

        $result = $em->createQuery(
            'SELECT g.id, g.genre from AppBundle:Genre g'
        )->getResult();

        $genres = array_column($result, 'genre', 'id');

        return $genres;
    }

    public function getGenreFromId($id)
    {
        $em = $this->getEntityManager();

        $result = $em->getRepository(Genre::class)->find($id);

        return $result;
    }

    public function getTopFiveBooks()
    {
        $em = $this->getEntityManager();
        $result = $em->getRepository(Book::class)->findBy(array('visible'=>'1'),array('views'=>'desc'),5);
        return $result;
    }

    public function getTopReviewBooks()
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT b.title,b.image, SUM(r.rating)/COUNT(r.id) as sumRating FROM AppBundle:Review r JOIN AppBundle:Book b WITH r.book_id=b.id GROUP BY r.book_id ORDER BY sumRating DESC');
        return $query->setMaxResults(5)->getResult();
    }

    public function getBooksFromGenre($id)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT b FROM AppBundle:Book b WHERE b.genre = :id  AND b.visible = 1'
        )->setParameter('id', $id);

        $result = $query->getResult();

        return $result;
    }

    public function getGenreID($genre)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT * FROM Genre')->getScalarResult();
        foreach($query as $ent)
        {
            if($ent->get == $genre)
            {
                return ;
            }

        }


    }

    public function getSumBook($id)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT SUM(r.rating) FROM AppBundle:Review r WHERE r.book_id = :id'
        )->setParameter('id',$id);
        return $query->getResult();
    }


    public function getBookFromId($id)
    {
        $em = $this->getEntityManager();

        return $em->getRepository(Book::class)->find($id);
    }

    public function getBooksReviews($bookId, $limit, $offset) {
        $em = $this->getEntityManager();

        $result = $em->getRepository(Review::class)->findBy(
            ['book_id' => $bookId],
            ['date' => 'ASC'],
            $limit,
            $offset
        );

        return $result;
    }

    public function getAllBooksReviews($bookId) {
        $em = $this->getEntityManager();

        $result = $em->getRepository(Review::class)->findBy(
            ['book_id' => $bookId],
            ['date' => 'ASC']
        );

        return $result;
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->entityManager;
    }
}