<?php

namespace AppBundle\Service;


use AppBundle\Entity\Book;
use AppBundle\Entity\Genre;
use AppBundle\Entity\Review;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;

class GoogleApiManager
{
    public function getBooksFromGoogleApi($search)
    {
        $api = new Client(['base_uri' => 'https://www.googleapis.com/books/v1/']);
        $response = $api->get('volumes?q=title=' . $search);

        $books = [];

        if ($response->getStatusCode() == 200) {
            $match = json_decode((string)$response->getBody(), true);

            if ($match['totalItems'] != 0) {
                foreach ($match["items"] as $marchKey => $marchValue) {
                    foreach ($marchValue as $volumeKey => $volumeValue) {
                        if(is_array($volumeValue)) {
                            if (array_key_exists("industryIdentifiers", $volumeValue)) {
                                array_push($books, [
                                    "isbn" => $volumeValue['industryIdentifiers'][0]['identifier'],
                                    "title" => $volumeValue["title"],
                                    "image" => $volumeValue["imageLinks"]['thumbnail']
                                ]);
                            }
                        }
                    }
                }
            }
        }
        return $books;
    }

    public function getBookInfoFromISBN($isbn)
    {
        $api = new Client(['base_uri' => 'https://www.googleapis.com/books/v1/']);
        $response = $api->get('volumes?q=isbn='.$isbn);

        $books = [];

        if ($response->getStatusCode() == 200) {
            $match = json_decode((string)$response->getBody(), true);

            if ($match['totalItems'] != 0) {
                foreach ($match["items"] as $marchKey => $marchValue) {
                    foreach ($marchValue as $volumeKey => $volumeValue) {
                        if (is_array($volumeValue)) {
                            if (array_key_exists("industryIdentifiers", $volumeValue)) {

                                array_push(
                                    $books,
                                    [
                                        "isbn" => $volumeValue['industryIdentifiers'][0]['identifier'],
                                        "title" => $volumeValue["title"],
                                        "image" => $volumeValue["imageLinks"]['thumbnail'],
                                        "author" => $volumeValue["authors"][0],
                                        "synopsis" => $volumeValue["description"],
                                        "genre" => $volumeValue['categories'][0]
                                    ]
                                );
                            }
                        }
                    }
                }
            }
        }

        return $books[0];
    }

    public function getBookRating($title)
    {
        $api = new Client(['base_uri' => 'https://www.googleapis.com/books/v1/']);
        $response = $api->get('volumes?q=title=' . $title);

        $averageRating = 0;

        if ($response->getStatusCode() == 200) {
            $match = json_decode((string)$response->getBody(), true);

            if ($match['totalItems'] != 0) {

                $bookItem = $match['items'][0];

                if(is_array($bookItem['volumeInfo'])) {
                    $bookInfo = $bookItem['volumeInfo'];

                    $averageRating = array_key_exists('averageRating', $bookInfo) ? $bookInfo['averageRating'] : "0";
                }
            }
        }
        return $averageRating;
    }
}